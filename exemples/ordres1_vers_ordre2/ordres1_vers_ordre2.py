import math
import acelectricity
import order2

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

print("""Mise en cascade de deux systèmes du 1er ordre
L'ensemble donne un système du 2ème ordre
""")

A1 = float(input("Amplification A1 ? "))
# fréquence de coupure à -3 dB
f1 = float(input("Fréquence de coupure f1 (Hz) ? "))
# pulsation en rad/s
w1 = 2*math.pi*f1
# constante de temps (s)
tau1 = 1/w1

A2 = float(input("Amplification A2 ? "))
# fréquence de coupure à -3 dB
f2 = float(input("Fréquence de coupure f2 (Hz) ? "))
# pulsation en rad/s
w2 = 2*math.pi*f2
# constante de temps (s)
tau2 = 1/w2

print("""1er filtre du 1er ordre :
Amplification :        {}
Fréquence de coupure : {} Hz
Pulsation de coupure : {} rad/s
Constante de temps :   {} s

2ème filtre du 1er ordre :
Amplification :        {}
Fréquence de coupure : {} Hz
Pulsation de coupure : {} rad/s
Constante de temps :   {} s
""".format(A1, f1, w1, tau1, A2, f2, w2, tau2))

# first order low-pass filter
H1 = acelectricity.Ratio(fw=lambda w: A1/(1+1j*w/w1))
H2 = acelectricity.Ratio(fw=lambda w: A2/(1+1j*w/w2))

# mise en cascade
H = H1*H2

# 4 décades min
H1.bode(xmin=min(f1, f2)/100, xmax=max(f1, f2)*100,
        title="1er filtre du 1er ordre")
H2.bode(xmin=min(f1, f2)/100, xmax=max(f1, f2)*100,
        title="2ème filtre du 1er ordre")
H.bode(xmin=min(f1, f2)/100, xmax=max(f1, f2)*100, title="2ème ordre")
acelectricity.show()

m, w0 = order2.Ordre2.abaque_m_w0_depuis_poles(-w1, -w2)
syst = order2.Ordre2(A=A1*A2, coeff_amortissement=m, w0=w0)

print("""
2ème ordre :
Coeff. d'amortissement : {}
Amplification :          {}
Gain statique :          {} dB
Fréquence propre :       {} Hz
Pulsation propre :       {} rad/s
Fréquence de coupure :   {} Hz
""".format(syst.amortissement, syst.amplification_statique,
           syst.gain_statique(),
           syst.frequence_propre(), syst.pulsation_propre,
           syst.frequence_coupure_harmonique()))

syst.bode(title="2ème ordre")
syst.show()

Hbis = syst.fonction_transfert

Hbis.properties(150)  # transmittance à 150 Hz
print(H.db(150))  # gain en dB à 150 Hz
print(H.phase_deg(150))  # déphasage en degrés à 150 Hz

# vérification
print(acelectricity.compare(Hbis, H))  # True
