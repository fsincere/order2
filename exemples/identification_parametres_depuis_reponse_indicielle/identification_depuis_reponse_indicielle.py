import math
import order2

__version__ = (0, 0, 3)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

print("""Identification des paramètres (A, m, w0) d'un système du 2ème ordre
à partir de sa réponse indicielle
(dans le cas d'un régime pseudo-périodique)
""")

A = float(input("Amplification statique ? "))

d1 = float(input("Premier dépassement en % ? "))
m = order2.Ordre2.abaque_coeff_amortissement(d1)
print("=> Coefficient d'amortissement :", m)

Tp = float(input("Pseudo-période en s ? "))
w0 = order2.Ordre2.abaque_pulsation_propre(coeff_amortissement=m,
                                           pseudo_periode=Tp)
print("=> Pulsation propre :", w0, "rad/s")

syst = order2.Ordre2(A=A, coeff_amortissement=m, w0=w0)
print("Fréquence propre :", syst.frequence_propre(), 'Hz')

print("Régime :", syst.regime())

t5 = syst.temps_de_reponse(5)
print("Temps_de_reponse à 5% :", t5, 's')
t1 = syst.temps_de_reponse(1)
print("Temps_de_reponse à 1% :", t1, 's')

# dépassements
for i in range(1, 11):
    di = syst.depassement(n=i)
    print("Dépassement n°", i, di, "%")

syst.courbe_reponse_indicielle(tmin=None, tmax=None)
syst.show()
