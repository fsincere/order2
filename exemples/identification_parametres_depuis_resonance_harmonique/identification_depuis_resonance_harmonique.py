import math
import order2

__version__ = (0, 0, 4)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

print("""Identification des paramètres (A, m, w0) d'un système du 2ème ordre
à partir de la réponse en fréquence
(dans le cas où il y a une résonance : 0 < m < 1/√2)
""")

G = float(input("Gain statique en dB ? "))
A = 10**(G/20)  # supposée positive
print("Amplification statique :", A)

print("Facteur de résonance = Gmax - Gstatique")
d_db = float(input("Facteur de résonance en dB ? "))
m = order2.Ordre2.abaque_coeff_amortissement_harmonique(facteur_resonance_db=d_db)
print("=> Coefficient d'amortissement :", m)

fr = float(input("Fréquence de résonance en Hz ? "))
w0 = order2.Ordre2.abaque_pulsation_propre_resonance_harmonique(coeff_amortissement=m, frequence_resonance=fr)
print("=> Pulsation propre :", w0, "rad/s")

syst = order2.Ordre2(A=A, coeff_amortissement=m, w0=w0)
print("Fréquence propre :", syst.frequence_propre(), 'Hz')
print("Régime :", syst.regime())
print("Fréquence de coupure à -3 dB :", syst.frequence_coupure_harmonique(), "Hz")
print("Fréquence de résonance :", syst.frequence_resonance_harmonique(), "Hz")
print("facteur de résonance : {} ou {} dB".format(*syst.facteur_resonance_harmonique()))

syst.bode(xunit="Hz", n=10000)
syst.bode(xunit="rad/s", n=10000)
syst.courbe_reponse_indicielle(tmin=None, tmax=None)
syst.show()
