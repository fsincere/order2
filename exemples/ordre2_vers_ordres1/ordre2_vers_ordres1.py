import math
import acelectricity
import order2

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

print(order2.__version__)

print("""Si le coefficient d'amortissement est supérieur à 1,
on peut décomposer un système du 2ème ordre en deux systèmes du 1er ordre
""")

m = float(input("Coeff. d'amortissement : ? "))
if m < 1:
    raise ValueError("Le coefficient d'amortissement doit être >= 1")
w0 = float(input("Pulsation propre (rad/s) ? "))

syst = order2.Ordre2(A=1, coeff_amortissement=m, w0=w0)

print("""
2ème ordre :
Coeff. d'amortissement : {}
Amplification :          {}
Gain statique :          {} dB
Fréquence propre :       {} Hz
Pulsation propre :       {} rad/s
Fréquence de coupure :   {} Hz
""".format(syst.amortissement, syst.amplification_statique,
           syst.gain_statique(),
           syst.frequence_propre(), syst.pulsation_propre,
           syst.frequence_coupure_harmonique()))

pole1, pole2 = syst.poles
w1, w2 = -pole1, -pole2
# fréquences de coupure
f1, f2 = w1/(2*math.pi), w2/(2*math.pi)
# constantes de temps
tau1, tau2 = 1/w1, 1/w2

print("""1er filtre du 1er ordre :
Amplification :        1
Fréquence de coupure : {} Hz
Pulsation de coupure : {} rad/s
Constante de temps :   {} s

2ème filtre du 1er ordre :
Amplification :        1
Fréquence de coupure : {} Hz
Pulsation de coupure : {} rad/s
Constante de temps :   {} s
""".format(f1, w1, tau1, f2, w2, tau2))

# first order low-pass filter
H1 = acelectricity.Ratio(fw=lambda w: 1/(1+1j*w/w1))
H2 = acelectricity.Ratio(fw=lambda w: 1/(1+1j*w/w2))

# mise en cascade
H = H1*H2

# 4 décades min
H1.bode(xmin=min(f1, f2)/100, xmax=max(f1, f2)*100,
        title="1er filtre du 1er ordre")
H2.bode(xmin=min(f1, f2)/100, xmax=max(f1, f2)*100,
        title="2ème filtre du 1er ordre")
H.bode(xmin=min(f1, f2)/100, xmax=max(f1, f2)*100, title="2ème ordre")
acelectricity.show()

syst.bode(title="2ème ordre")
syst.show()

Hbis = syst.fonction_transfert

Hbis.properties(150)  # transmittance à 150 Hz
print(H.db(150))  # gain en dB à 150 Hz
print(H.phase_deg(150))  # déphasage en degrés à 150 Hz

# vérification
print(acelectricity.compare(Hbis, H))  # True
