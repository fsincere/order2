import math
import order2

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

syst = order2.Ordre2(A=10, coeff_amortissement=0.1, w0=10000)
print("fréquence propre :", syst.frequence_propre(), 'Hz')

f = 0
T = syst.fonction_transfert_complexe(f)
print("fonction de transfert à {} Hz : {}".format(f, T))
H = syst.fonction_transfert
print("Gain à {} Hz : {} dB".format(f, H.db(f)))
f = 1000
T = syst.fonction_transfert_complexe(f)
print("fonction de transfert à {} Hz : {}".format(f, T))
H.properties(1000)

print("facteur de résonance : {} ou {} dB".format(*syst.facteur_resonance_harmonique()))
print("fréquence de résonance : {} Hz".format(syst.frequence_resonance_harmonique()))

fc = syst.frequence_coupure_harmonique()
print("fréquence de coupure à -3 dB : {} Hz".format(fc))
H.properties(fc)

syst.bode(xunit="Hz")
syst.bode(xunit="rad/s")
syst.show()
