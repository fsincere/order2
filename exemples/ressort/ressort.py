import math
import order2

syst = order2.MKF(M=2, K=200, f=12.8)
print("régime :", syst.regime())
print("coefficient d'amortissement :", syst.amortissement)
print("premier dépassement en %", syst.depassement())
print("pseudo-période", syst.pseudo_periode())
print("temps_de_reponse à 5%", syst.temps_de_reponse(5))
print("pulsation propre en rad/s :", syst.pulsation_propre)

syst.courbe_reponse_indicielle()
syst.show()

syst = order2.MKF(M=2, K=200, f=100)
print("régime :", syst.regime())
print("coefficient d'amortissement :", syst.amortissement)
print("temps_de_reponse à 5%", syst.temps_de_reponse(5))
print("pulsation propre en rad/s :", syst.pulsation_propre)

syst.courbe_reponse_indicielle()
syst.show()
