import math
import order2

__version__ = (0, 0, 3)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

syst = order2.Ordre2(A=2, coeff_amortissement=0.05, w0=1000)
print("fréquence propre :", syst.frequence_propre(), 'Hz')

print("régime :", syst.regime())

t5 = syst.temps_de_reponse(5)
print("temps_de_reponse à 5% :", t5, 's')
t1 = syst.temps_de_reponse(1)
print("temps_de_reponse à 1% :", t1, 's')

print(syst.reponse_indicielle(t1))

if syst.amortissement < 1:

    print("pseudo-période :", syst.pseudo_periode(), 's')

    print("premier dépassement :", syst.depassement(), "%")

    # dépassements
    for i in range(1, 11):
        di = syst.depassement(n=i)
        print("dépassement n°", i, di, "%")

syst.courbe_reponse_indicielle()
syst.courbe_reponse_indicielle(-0.01, 0.20)
syst.show()
