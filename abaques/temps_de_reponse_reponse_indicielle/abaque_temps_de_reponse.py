# -*- coding: utf8 -*-
# python 3
# test OK

import order2

try:
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FuncFormatter
except ImportError:
    print("Vous devez installer le package matplotlib")
    exit(1)

__version__ = (0, 0, 6)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"


pourcentage = float(input("pourcentage ? "))

fig, axe = plt.subplots(1, 1)

axe.grid(True, which="both")

axe.set_title("Temps de réponse réduit {} %".format(pourcentage), fontsize=14)
axe.set_xlabel("Coefficient d'amortissement")
axe.set_ylabel(r'$\omega_0.tr_{{{}\%}}$'.format(pourcentage))

# echelle log
m_values = np.geomspace(0.01, 100, 10000)
# m_values = np.geomspace(0.05, 5, 10000)

tr = []
for m in m_values:
    tr.append(order2.Ordre2.abaque_temps_de_reponse(m, 1, pourcentage))

print("au minimum :")
print("w0.tr = ", min(tr))
indice = tr.index(min(tr))
print("Coefficient d'amortissement", m_values[indice])

axe.loglog(m_values, tr, 'b-', linewidth=2.0)

for axis in [axe.xaxis, axe.yaxis]:
    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    axis.set_major_formatter(formatter)

fig.tight_layout()
plt.show()
