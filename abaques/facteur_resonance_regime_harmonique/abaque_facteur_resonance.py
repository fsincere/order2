# -*- coding: utf8 -*-
# python 3
# test OK

import order2

try:
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.ticker import AutoMinorLocator
except ImportError:
    print("Vous devez installer le package matplotlib")
    exit(1)

__version__ = (0, 0, 4)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

fig, axe = plt.subplots(1, 1)

axe.grid(True, which="both")

axe.set_title("Résonance en régime harmonique", fontsize=14)
axe.set_xlabel("Coefficient d'amortissement")
axe.set_ylabel("Facteur de résonance (dB)")
axe.set_xlim([0, 2**-0.5])
axe.set_ylim([0, 40])
axe.xaxis.set_minor_locator(AutoMinorLocator())
axe.yaxis.set_minor_locator(AutoMinorLocator())

m_values = np.linspace(1e-300, 2**-0.5, 1000)

d1 = []
for m in m_values:
    d1.append(order2.Ordre2.abaque_facteur_resonance_harmonique(coeff_amortissement=m)[1])

axe.plot(m_values, d1, 'b-', linewidth=2.0)

fig.tight_layout()
plt.show()
