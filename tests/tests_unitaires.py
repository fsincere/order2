import math
import unittest
import order2

__version__ = (0, 0, 6)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

print(order2.__version__)

if order2.__version__ < (0, 3, 9):
    print('You need to run this with order2 >= 0.3.9')
    exit(1)


class Test_recherche_solution(unittest.TestCase):
    def test(self):
        def g(x):
            return x*x
        solution = order2._rechercheSolution(0, 2, 2, 1e-12, g)
        self.assertAlmostEqual(solution, 2**0.5)


class Tests_ordre2(unittest.TestCase):

    def test_gain_statique(self):
        syst = order2.Ordre2(A=-100, coeff_amortissement=0.16, w0=5000)
        self.assertAlmostEqual(syst.gain_statique(), 40)

    def test_proprietes(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.16, w0=5000)
        self.assertAlmostEqual(syst.amortissement, 0.16)
        self.assertAlmostEqual(syst.amplification_statique, -10)
        self.assertAlmostEqual(syst.pulsation_propre, 5000)

        # lecture seule
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.amortissement = 1.1
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.amplification_statique = 1.1
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.pulsation_propre = 1.1
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.fonction_transfert = 1.1

    def test_regime(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.16, w0=5000)
        self.assertEqual(syst.regime(), 'pseudo-périodique')
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.0, w0=5000)
        self.assertEqual(syst.regime(), 'oscillant')
        syst = order2.Ordre2(A=-10, coeff_amortissement=1.0, w0=5000)
        self.assertEqual(syst.regime(), 'critique')
        syst = order2.Ordre2(A=-10, coeff_amortissement=1.16, w0=5000)
        self.assertEqual(syst.regime(), 'apériodique')

    def test_frequence_propre(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.16, w0=5000)
        self.assertAlmostEqual(syst.frequence_propre(), 5000/(2*math.pi))

    def test_temps_de_reponse(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.16, w0=5000)
        temps_de_reponse = syst.temps_de_reponse(5)
        v = syst.reponse_indicielle(temps_de_reponse)
        self.assertAlmostEqual(v, -10*1.05)
        temps_de_reponse = syst.temps_de_reponse(1)
        v = syst.reponse_indicielle(temps_de_reponse)
        self.assertAlmostEqual(v, -10*1.01)

    def test_abaque_temps_de_reponse(self):
        syst = order2.Ordre2(A=50, coeff_amortissement=1000, w0=5000)
        temps_de_reponse = syst.temps_de_reponse(5)
        t = order2.Ordre2.abaque_temps_de_reponse(1000, 5000, 5)
        self.assertAlmostEqual(temps_de_reponse, t)
        t = order2.Ordre2.abaque_temps_de_reponse(0.1, 5000, 100)
        self.assertEqual(t, 0)

        t = order2.Ordre2.abaque_temps_de_reponse(0, 5000, 0.0001)
        self.assertEqual(t, float('inf'))
        t = order2.Ordre2.abaque_temps_de_reponse(0.01, 5000, 0.0001)
        self.assertEqual(t, 0.27588860024709205)
        t = order2.Ordre2.abaque_temps_de_reponse(0.9999, 5000, 0.0001)
        self.assertEqual(t, 0.00333582505040616)
        t = order2.Ordre2.abaque_temps_de_reponse(1, 5000, 0.0001)
        self.assertEqual(t, 0.003337684156373143)
        t = order2.Ordre2.abaque_temps_de_reponse(100, 5000, 0.0001)
        self.assertEqual(t, 0.5526076067239046)
        t = order2.Ordre2.abaque_temps_de_reponse(1e6, 5000, 0.0001)
        self.assertEqual(t, 5526.159096509218)

    def test_pseudo_periode(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0, w0=5000)
        T = syst.pseudo_periode()
        f0 = syst.frequence_propre()
        self.assertAlmostEqual(T, 1/f0)

    def test_depassement(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0, w0=5000)
        self.assertAlmostEqual(syst.depassement(1), 100)
        syst = order2.Ordre2(A=-10, coeff_amortissement=1, w0=5000)
        self.assertAlmostEqual(syst.depassement(1), 0)

    def test_frequence_resonance(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0, w0=5000)
        self.assertAlmostEqual(syst.frequence_resonance_harmonique(),
                               syst.frequence_propre())
        syst = order2.Ordre2(A=-10, coeff_amortissement=1, w0=5000)
        self.assertAlmostEqual(syst.frequence_resonance_harmonique(), 0)

    def test_frequence_coupure(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.01, w0=1000)
        fc = syst.frequence_coupure_harmonique()
        T = syst.fonction_transfert_complexe(fc)
        self.assertAlmostEqual(abs(T), 10/2**0.5)
        syst = order2.Ordre2(A=100, coeff_amortissement=1200, w0=1000)
        fc = syst.frequence_coupure_harmonique()
        T = syst.fonction_transfert_complexe(fc)
        self.assertAlmostEqual(abs(T), 100/2**0.5)
        H = syst.fonction_transfert
        self.assertAlmostEqual(40-10*math.log10(2), H.db(fc))

    def test_facteur_resonance(self):
        syst = order2.Ordre2(A=-10, coeff_amortissement=0.01, w0=1000)
        fr = syst.frequence_resonance_harmonique()
        A, db = syst.facteur_resonance_harmonique()
        T = syst.fonction_transfert_complexe(fr)
        self.assertAlmostEqual(abs(T)/10, A)
        H = syst.fonction_transfert
        self.assertAlmostEqual(db+20, H.db(fr))

    def test_abaque_pulsation_propre(self):
        T = order2.Ordre2.abaque_pseudo_periode(coeff_amortissement=0, w0=5000)
        self.assertAlmostEqual(T, 2*math.pi/5000)

        T = order2.Ordre2.abaque_pseudo_periode(coeff_amortissement=0.26,
                                                w0=5000)
        w = order2.Ordre2.abaque_pulsation_propre(coeff_amortissement=0.26,
                                                  pseudo_periode=T)
        self.assertAlmostEqual(w, 5000)

        w = order2.Ordre2.abaque_pulsation_propre(coeff_amortissement=0.26,
                                                  pseudo_periode=0.158)
        T = order2.Ordre2.abaque_pseudo_periode(coeff_amortissement=0.26, w0=w)
        self.assertAlmostEqual(T, 0.158)

    def test_abaque_abaque_coeff_amortissement_harmonique(self):

        m = order2.Ordre2.abaque_coeff_amortissement_harmonique(
            facteur_resonance_db=100000)
        self.assertEqual(m, 0.0)

        m = order2.Ordre2.abaque_coeff_amortissement_harmonique(
            facteur_resonance_db=12.5)
        db = order2.Ordre2.abaque_facteur_resonance_harmonique(
            coeff_amortissement=m)[1]
        self.assertAlmostEqual(db, 12.5)

        db = order2.Ordre2.abaque_facteur_resonance_harmonique(
            coeff_amortissement=0.001)[1]
        m = order2.Ordre2.abaque_coeff_amortissement_harmonique(
            facteur_resonance_db=db)
        self.assertAlmostEqual(m, 0.001)

        m = order2.Ordre2.abaque_coeff_amortissement_harmonique(
            facteur_resonance_db=1000)
        self.assertAlmostEqual(m, 5e-51)

        m = order2.Ordre2.abaque_coeff_amortissement_harmonique(
            facteur_resonance_db=100)
        self.assertAlmostEqual(m, 5.000000206850923e-06)

        m = order2.Ordre2.abaque_coeff_amortissement_harmonique(
            facteur_resonance_db=100.1)
        self.assertAlmostEqual(m, 4.942765473284695e-06)

    def test_abaque_depassement(self):
        d1 = order2.Ordre2.abaque_depassement(coeff_amortissement=0.138, n=1)
        m = order2.Ordre2.abaque_coeff_amortissement(depassement=d1)
        self.assertAlmostEqual(m, 0.138)

        m = order2.Ordre2.abaque_coeff_amortissement(depassement=77.1)
        d1 = order2.Ordre2.abaque_depassement(coeff_amortissement=m, n=1)
        self.assertAlmostEqual(d1, 77.1)

    def test_abaque_frequence_resonance_harmonique(self):
        fr = order2.Ordre2.abaque_frequence_resonance_harmonique(
            coeff_amortissement=0.2, w0=1200)
        w0 = order2.Ordre2.abaque_pulsation_propre_resonance_harmonique(
            coeff_amortissement=0.2, frequence_resonance=fr)
        self.assertAlmostEqual(w0, 1200)

        w0 = order2.Ordre2.abaque_pulsation_propre_resonance_harmonique(
            coeff_amortissement=0.6, frequence_resonance=1000)
        fr = order2.Ordre2.abaque_frequence_resonance_harmonique(
            coeff_amortissement=0.6, w0=w0)
        self.assertAlmostEqual(fr, 1000)

    def test_warnings(self):

        with self.assertWarns(UserWarning):
            # coeff_amortissement > 100
            order2.Ordre2.abaque_frequence_coupure_harmonique(
                coeff_amortissement=200, w0=100)

        with self.assertWarns(UserWarning):
            order2.Ordre2.abaque_coeff_amortissement_harmonique(
                facteur_resonance_db=2000)

        with self.assertWarns(UserWarning):
            order2.Ordre2.abaque_facteur_resonance_harmonique(
                coeff_amortissement=0)

        with self.assertWarns(UserWarning):
            order2.Ordre2.abaque_facteur_resonance_harmonique(
                coeff_amortissement=0.708)

        with self.assertWarns(UserWarning):
            order2.Ordre2.abaque_temps_de_reponse(
                coeff_amortissement=0, w0=10, pourcentage=1)

        with self.assertWarns(UserWarning):
            order2.Ordre2.abaque_temps_de_reponse(
                coeff_amortissement=0, w0=10, pourcentage=5)

        with self.assertWarns(UserWarning):
            order2.Ordre2.abaque_pulsation_propre_resonance_harmonique(
                coeff_amortissement=0.708, frequence_resonance=100)


class Tests_RLC(unittest.TestCase):

    def test(self):
        syst = order2.RLC(R=0, L=0.01, C=100e-9)
        self.assertAlmostEqual(syst.amortissement, 0)
        syst = order2.RLC(R=100, L=0.01, C=100e-9)
        self.assertAlmostEqual(syst.amplification_statique, 1)
        Rc = syst.resistance_critique()
        Lc = syst.inductance_critique()
        Cc = syst.capacite_critique()
        syst = order2.RLC(R=Rc, L=0.01, C=100e-9)
        self.assertAlmostEqual(syst.amortissement, 1)
        syst = order2.RLC(R=100, L=Lc, C=100e-9)
        self.assertAlmostEqual(syst.amortissement, 1)
        syst = order2.RLC(R=100, L=0.01, C=Cc)
        self.assertAlmostEqual(syst.amortissement, 1)

        # lecture seule
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.R = 1.1
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.L = 1.1
        with self.assertRaises(AttributeError):
            # AttributeError: can't set attribute
            syst.C = 1.1

        t = order2.RLC.abaque_temps_de_reponse(0.1, 5000, 100)
        self.assertEqual(t, 0)


if __name__ == '__main__':
    unittest.main(verbosity=1)
